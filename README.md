# Fastify Crud API

This application use fastify as a framework of node js and performing basic action (CRUD) of api to get better understanding of fastify  

## Usage

Run this project on your machine you have to perform following steps :-

```
npm i/install

npm run dev
```

### Swagger docs

Visit http://localhost:3000/docs

### REST Client

It's same as thunderclient and easy to use 

The file **test.http** can be used to make requests if you are using the [VSCode Rest Client extension](https://marketplace.visualstudio.com/items?itemName=humao.rest-client)
