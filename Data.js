let indianLanguage = [
  { id: '1', language: 'Hindi', region: 'Uttar Pradesh', majority: '90%' },
  { id: '2', language: 'Kannada', region: 'Karnatka', majority: '66%' },
  { id: '3', language: 'Malayalam', region: 'Kerla', majority: '96.6%' },
  { id: '4', language: 'Telugu', region: 'Andra Pradesh', majority: '84%' },
]

module.exports = {
  indianLanguage,
}
 