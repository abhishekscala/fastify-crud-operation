const fastify = require('fastify')({ logger: true })

//enable the swagger documentation for every single api
fastify.register(require('fastify-swagger'), {
  exposeRoute: true,
  routePrefix: '/docs',
  swagger: {
    info: { title: 'language-api' },
  },
})

// register routes
fastify.register(require('./routes/languageRoutes'))


const PORT = 3000

const start = async () => {
  try {
    await fastify.listen(PORT)
  } catch (error) {
    fastify.log.error(error)
    process.exit(1)
  }
}

start()
