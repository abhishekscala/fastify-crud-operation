const { v4: uuidv4 } = require('uuid')
let { indianLanguage} = require('../Data')


const getLanguages = (req, res) => {
  res.send(indianLanguage)
}

const getLanguage = (req, res) => {
  const { id } = req.params

  const getSelectedIndianLanguage = indianLanguage.find((language) => language.id === id)

  res.send(getSelectedIndianLanguage)
}

const addLanguage = (req, res) => {
  const { language, region,  majority } = req.body
  const newIndianLanguage = {
    id: uuidv4(),
    language,
    region,
    majority,
  }

  indianLanguage = [...indianLanguage, newIndianLanguage]
  res.code(201).send(indianLanguage)
}

const deleteLanguage = (req, res) => {
  const { id } = req.params

  indianLanguage = indianLanguage.filter((item) => item.id !== id)

  res.send({ message: `Indian Language  ${id} has been removed` })
}

const updateLanguage = (req, res) => {
  const { id } = req.params
  const { language, region,  majority } = req.body

  indianLanguage = indianLanguage.map((singleLanguage) => (singleLanguage.id === id ? { id, language, region,  majority } : singleLanguage))

  updateIndianLangauge = indianLanguage.find((language) => language.id === id)

  res.send(updateIndianLangauge)
}

module.exports = {
  getLanguages,
  getLanguage,
  addLanguage,
  deleteLanguage,
  updateLanguage,
}
