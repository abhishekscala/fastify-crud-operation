const {
  getLanguages,
  getLanguage,
  addLanguage,
  deleteLanguage,
  updateLanguage,
} = require('../controllers/languageController')


// Language schema
const IndianLanguage = {
  type: 'object',
  properties: {
    id: { type: 'string' },
    language: { type: 'string' },
    region: { type: 'string' },
    majority: { type: 'string' },
  },
}

// Options for get all languages
const getIndianLanguagesOpts = {
  schema: {
    response: {
      200: {
        type: 'array',
        languages: IndianLanguage,
      },
    },
  },
  handler: getLanguages,
}
// Options for single language
const getInidanLanguageOpts = {
  schema: {
    response: {
      200: IndianLanguage,
    },
  },
  handler: getLanguage,
}
// Options for add new language
const postInidanLanguageOpts = {
  schema: {
    body: {
      type: 'object',
      required: ['language', 'region', 'majority'],
      properties: {
        language: { type: 'string' },
        region: { type: 'string' },
        majority: { type: 'string' },
      },
    },
    response: {
      201: {
        type: 'array',
        languages: IndianLanguage,
      },
    },
  },
  handler: addLanguage,
}
// Options for delete language
const deleteInidanLanguageOpts = {
  schema: {
    response: {
      200: {
        type: 'object',
        properties: {
          message: { type: 'string' },
        },
      },
    },
  },
  handler: deleteLanguage,
}
// Options for update language
const updateInidanLanguageOpts = {
  schema: {
    body: {
      type: 'object',
      required: ['language', 'region', 'majority'],
      properties: {
        language: { type: 'string' },
        region: { type: 'string' },
        majority: { type: 'string' },
      },
    },
    response: {
      200: IndianLanguage,
    },
  },
  handler: updateLanguage,
}

function languageRoutes(fastify, options, done) {
  // Get all languages
  fastify.get('/languages', getIndianLanguagesOpts)

  // Get single language
  fastify.get('/language/:id', getInidanLanguageOpts)

  // Add languages
  fastify.post('/language', postInidanLanguageOpts)

  // Delete languages
  fastify.delete('/language/delete/:id', deleteInidanLanguageOpts)

  // Update languages
  fastify.put('/language/update/:id', updateInidanLanguageOpts)

  done()
}

module.exports = languageRoutes
